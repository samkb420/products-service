const express = require("express");
const app = express();
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const amqp = require("amqplib");
const cors = require("cors");

const Product = require("./models/Product");
var order
const isAuthenticated = require("./auth/isAuthenticated");

var channel, connection;

const DBURL =
  "mongodb+srv://sam:12345678sK@cluster0.ubll2.mongodb.net/product-service?retryWrites=true&w=majority";

// mongo
mongoose.connect(
  DBURL,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("Product-service Connected  to mongodb");
    }
  }
); // connect to mongoDB database called 'product-service'

app.use(express.json());
app.use(cors())

app.get("/", (req, res) => {
  res.json([
    {
      "Service Name": "Product Service",
      "Service URL": "http://localhost:808000",
    },
    { Status: "Running" },
  ]);
});

async function connect() {
  const amqpServer =
    "amqps://nebdjztn:c6Qv1n5-QyBYu9X0mESdufGyaK8lPUFM@chimpanzee.rmq.cloudamqp.com/nebdjztn";

  connection = await amqp.connect(amqpServer);
  channel = await connection.createChannel();

  await channel.assertQueue("PRODUCT_QUEUE");
}

connect();

// create a new product
app.post("/api/v1/add-product", isAuthenticated, async (req, res) => {
  const { name, description, price, image } = req.body;

    const product = new Product({
        name,
        description,
        price,
        image,
    });
      product.save()
    return res.json(product);

});

app.get("/api/v1/get-products",async (req,res) =>{

 try{
    const products = await Product.find()

   res.status(200).json(products);

 }
 catch(error){

   res.send(400).json(error);

 }



});

// user sends a list of product to buy

// creating a new order and totoal price of the order

app.post('/api/v1/create-order', isAuthenticated, async (req, res) => {
   const {ids } = req.body;
    const products = await Product.find({_id: {$in: ids}});

    channel.sendToQueue("ORDER_QUEUE", Buffer.from(JSON.stringify({
        products,
        userEmail: req.user.email,
    })));

    channel.consume("PRODUCT_QUEUE", data => {
      console.log("Conduming Product Queue"); 
       order = JSON.parse(data.content);
       channel.ack(data);

    });

    return res.json(order);

  });

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Products server listening on port ${PORT}`);
});
