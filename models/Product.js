// Product model

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ProductSchema = new Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    price: { type: Number, required: true },
    image: { type: String, required: true },
    create_at: { type: Date, default: Date.now },

})

module.exports = Product = mongoose.model('product', ProductSchema);